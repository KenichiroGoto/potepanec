class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.products
    @taxonomy = Spree::Taxonomy.category
    @categories = @taxonomy.taxons.leaves.includes(:products)
  end
end
