class Potepan::TopController < ApplicationController
  def index
    @featured_products = Spree::Product.order(available_on: :desc).limit(8)
  end
end
