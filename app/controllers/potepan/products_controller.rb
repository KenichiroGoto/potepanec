class Potepan::ProductsController < ApplicationController
  NUMBER_OF_ITEMS_DISPLAYS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.category_and_brand_related_products( NUMBER_OF_ITEMS_DISPLAYS )
  end
end
