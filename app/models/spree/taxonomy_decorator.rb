Spree::Taxonomy.class_eval do

  def self.category
    find_by( name: "Categories" )
  end

  def self.brand
    find_by( name: "Brand" )
  end

end
