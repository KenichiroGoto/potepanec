Spree::Taxon.class_eval do

  scope :categories, -> { where( taxonomy: Spree::Taxonomy.category ) }
  scope :brands, -> { where( taxonomy: Spree::Taxonomy.brand ) }

end
