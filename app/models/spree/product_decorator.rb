Spree::Product.class_eval do

  scope :without_product, -> (product) { where.not(id: product.id) }
  scope :related_taxons, -> (taxons) { includes(:taxons).where(spree_taxons: { id: taxons&.ids }) }

  def related_products(taxons)
    Spree::Product.related_taxons(taxons).without_product(self)
  end

  def category_and_brand_related_products(num)
    ( related_products(taxons.categories).sample(num) + related_products(taxons.brands).sample(num) ).uniq
  end

end
