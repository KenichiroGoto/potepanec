require 'rails_helper'

describe Potepan::ProductsController, type: :controller do

  describe 'GET #show' do

    # taxonomy: 1 - taxon: 1 - product: 1
    #             ＼           product: 2
    #               taxon: 2 ∠ product: 3
    # taxonomy: 2 - taxon: 3 ∠ product: 4

    describe 'responseの検証' do

      let(:taxonomy_1)        { create(:taxonomy, name: "Categories") }
      let(:taxonomy_2)        { create(:taxonomy, name: "Brand") }
      let(:product_1)         { create(:product) }
      let(:product_2)         { create(:product) }
      let(:product_3)         { create(:product) }
      let(:product_4)         { create(:product) }
      let(:taxon_1)           { create(:taxon, taxonomy: taxonomy_1) }
      let(:taxon_2)           { create(:taxon, taxonomy: taxonomy_1) }
      let(:taxon_3)           { create(:taxon, taxonomy: taxonomy_2) }
      let!(:classification_1) { create(:classification, product: product_1, taxon: taxon_1) }
      let!(:classification_2) { create(:classification, product: product_2, taxon: taxon_2) }
      let!(:classification_3) { create(:classification, product: product_3, taxon: taxon_2) }
      let!(:classification_4) { create(:classification, product: product_3, taxon: taxon_3) }
      let!(:classification_5) { create(:classification, product: product_4, taxon: taxon_3) }

      context '指定した商品が存在する場合' do
        it '200 OKとなること' do
          get :show, params: { id: product_1.id }
          expect(response.status).to eq 200
        end

        it 'showテンプレートを表示すること' do
          get :show, params: { id: product_1.id }
          expect(response).to render_template :show
        end
      end

      context '指定した商品が存在しない場合' do
        it 'リクエストはRecordNotFoundとなること' do
          expect{ get :show, params: {id: 0}}.to raise_exception{ ActiveRecord::RecordNotFound }
        end
      end

    end

    describe '@productの検証' do
      let(:taxonomy_1)        { create(:taxonomy, name: "Categories") }
      let(:taxonomy_2)        { create(:taxonomy, name: "Brand") }
      let(:product_1)         { create(:product) }
      let(:product_2)         { create(:product) }
      let(:product_3)         { create(:product) }
      let(:product_4)         { create(:product) }
      let(:taxon_1)           { create(:taxon, taxonomy: taxonomy_1) }
      let(:taxon_2)           { create(:taxon, taxonomy: taxonomy_1) }
      let(:taxon_3)           { create(:taxon, taxonomy: taxonomy_2) }
      let!(:classification_1) { create(:classification, product: product_1, taxon: taxon_1) }
      let!(:classification_2) { create(:classification, product: product_2, taxon: taxon_2) }
      let!(:classification_3) { create(:classification, product: product_3, taxon: taxon_2) }
      let!(:classification_4) { create(:classification, product: product_3, taxon: taxon_3) }
      let!(:classification_5) { create(:classification, product: product_4, taxon: taxon_3) }

      context '指定した商品が存在する場合' do
        context 'CategoriesとBrandの関連productがある場合' do
          it '@product,@related_productsに指定したproductデータが入っていること' do
            get :show, params: { id: product_3.id }
            expect(assigns(:product)).to eq product_3
            expect(assigns(:related_products)).to include product_2, product_4
          end
        end

        context 'Categoriesの関連productがある場合' do
          it '@product,@related_productsに指定したproductデータが入っていること' do
            get :show, params: { id: product_2.id }
            expect(assigns(:product)).to eq product_2
            expect(assigns(:related_products).count).to eq 1
            expect(assigns(:related_products)).to include product_3
          end
        end

        context 'Brandの関連productがある場合' do
          it '@product,@related_productsに指定したproductデータが入っていること' do
            get :show, params: { id: product_4.id }
            expect(assigns(:product)).to eq product_4
            expect(assigns(:related_products).count).to eq 1
            expect(assigns(:related_products)).to include product_3
          end
        end
      end

    end

  end

end
