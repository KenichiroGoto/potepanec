require 'rails_helper'

describe Potepan::CategoriesController, type: :controller do

  describe 'GET #show' do

    # taxonomy: 1 - taxon: 1 - product: 1
    #             ＼           product: 2
    #               taxon: 2 ∠ product: 3
    # taxonomy: 2 - taxon: 3 ∠ product: 4

    describe 'responseの検証' do

      let(:taxonomy_1)        { create(:taxonomy, name: "Categories") }
      let(:taxonomy_2)        { create(:taxonomy, name: "Brand") }
      let(:product_1)         { create(:product) }
      let(:product_2)         { create(:product) }
      let(:product_3)         { create(:product) }
      let(:product_4)         { create(:product) }
      let(:taxon_1)           { create(:taxon, taxonomy: taxonomy_1) }
      let(:taxon_2)           { create(:taxon, taxonomy: taxonomy_1) }
      let(:taxon_3)           { create(:taxon, taxonomy: taxonomy_2) }
      let!(:classification_1) { create(:classification, product: product_1, taxon: taxon_1) }
      let!(:classification_2) { create(:classification, product: product_2, taxon: taxon_2) }
      let!(:classification_3) { create(:classification, product: product_3, taxon: taxon_2) }
      let!(:classification_4) { create(:classification, product: product_3, taxon: taxon_3) }
      let!(:classification_5) { create(:classification, product: product_4, taxon: taxon_3) }

      it '200 OKとなること' do
        get :show, params: { id: taxon_1.id }
        expect(response.status).to eq 200
      end

      it 'showテンプレートを表示すること' do
        get :show, params: { id: taxon_1.id }
        expect(response).to render_template :show
      end

    end

    describe '@taxon,@products,@taxonomy,@categoriesの検証' do

      let(:taxonomy_1)        { create(:taxonomy, name: "Categories") }
      let(:taxonomy_2)        { create(:taxonomy, name: "Brand") }
      let(:product_1)         { create(:product) }
      let(:product_2)         { create(:product) }
      let(:product_3)         { create(:product) }
      let(:product_4)         { create(:product) }
      let(:taxon_1)           { create(:taxon, taxonomy: taxonomy_1) }
      let(:taxon_2)           { create(:taxon, taxonomy: taxonomy_1) }
      let(:taxon_3)           { create(:taxon, taxonomy: taxonomy_2) }
      let!(:classification_1) { create(:classification, product: product_1, taxon: taxon_1) }
      let!(:classification_2) { create(:classification, product: product_2, taxon: taxon_2) }
      let!(:classification_3) { create(:classification, product: product_3, taxon: taxon_2) }
      let!(:classification_4) { create(:classification, product: product_3, taxon: taxon_3) }
      let!(:classification_5) { create(:classification, product: product_4, taxon: taxon_3) }

      it '@taxonに意図したデータが入っていること' do
        get :show, params: { id: taxon_1.id }
        expect(assigns(:taxon)).to eq taxon_1
      end

      it '@productsに意図したデータが入っていること' do
        get :show, params: { id: taxon_2.id }
        expect(assigns(:taxon)).to eq taxon_2
        expect(assigns(:products).count).to eq 2
        expect(assigns(:products)).to include product_2, product_3
      end

      it '@taxonomyに意図したデータが入っていること' do
        get :show, params: { id: taxon_1.id }
        expect(assigns(:taxonomy)).to eq taxonomy_1
      end

      it '@categoriesに意図したデータが入っていること' do
        get :show, params: { id: taxon_1.id }
        expect(assigns(:categories)).to include taxon_1, taxon_2
      end

    end

  end

end
