require 'rails_helper'

describe Spree::Taxon, type: :model do

  describe '#categories' do

    #               taxon: 1
    # taxonomy: 1 ∠ taxon: 2
    # taxonomy: 2 - taxon: 3

    let(:taxonomy_1)      { create(:taxonomy, name: "Categories") }
    let(:taxonomy_2)      { create(:taxonomy, name: "Brand") }
    let(:taxon_1)         { create(:taxon, taxonomy: taxonomy_1) }
    let(:taxon_2)         { create(:taxon, taxonomy: taxonomy_1) }
    let!(:taxon_3)        { create(:taxon, taxonomy: taxonomy_2) }
    let(:taxon_categories)  { Spree::Taxon.categories}

    context 'Spree::Taxonに taxonomy_name: Categories のデータが存在する場合' do
      it 'scopeしたデータがtaxon_1,taxon_2を含んでいる事' do
        taxon_1
        taxon_2
        expect(taxon_categories).to include taxon_1, taxon_2
      end
    end

    context 'Spree::Taxonに taxonomy_name: Categories のデータが存在しない場合' do
      it 'scopeの結果が空配列[]であること' do
        expect(taxon_categories).to eq []
      end
    end

  end

  describe '#brands' do

    #               taxon: 1
    # taxonomy: 1 ∠ taxon: 2
    # taxonomy: 2 - taxon: 3

    let(:taxonomy_1)    { create(:taxonomy, name: "Brand") }
    let(:taxonomy_2)    { create(:taxonomy, name: "Categories") }
    let(:taxon_1)       { create(:taxon, taxonomy: taxonomy_1) }
    let(:taxon_2)       { create(:taxon, taxonomy: taxonomy_1) }
    let!(:taxon_3)      { create(:taxon, taxonomy: taxonomy_2) }
    let(:taxon_brands)  { Spree::Taxon.brands }

    context 'Spree::Taxonに taxonomy_name: Brand のデータが存在する場合' do
      it 'scopeしたデータがtaxon_1,taxon_2を含んでいる事' do
        taxon_1
        taxon_2
        expect(taxon_brands).to include taxon_1, taxon_2
      end
    end

    context 'Spree::Taxonに taxonomy_name: Brand のデータが存在しない場合' do
      it 'scopeの結果が空配列[]であること' do
        expect(taxon_brands).to eq []
      end
    end

  end

end
