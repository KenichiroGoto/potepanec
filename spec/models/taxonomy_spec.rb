require 'rails_helper'

describe Spree::Taxonomy, type: :model do

  describe '#category' do
    let(:taxonomy_1)        { create(:taxonomy, name: "Categories") }
    let!(:taxonomy_2)       { create(:taxonomy, name: "Brand") }
    let(:taxonomy_category) { Spree::Taxonomy.category }

    context 'Spree::Taxonomyにname: "Categories"のデータが存在する場合' do
      it 'メソッドの戻り値.nameが"Categories"であること' do
        taxonomy_1
        expect(taxonomy_category.name).to eq "Categories"
      end
    end

    context 'Spree::Taxonomyにname: "Categories"のデータが存在しない場合' do
      it 'メソッドの戻り値がnilであること' do
        expect(taxonomy_category).to eq nil
      end
    end

  end

  describe '#brand' do
    let!(:taxonomy_1)    { create(:taxonomy, name: "Categories") }
    let(:taxonomy_2)     { create(:taxonomy, name: "Brand") }
    let(:taxonomy_brand) { Spree::Taxonomy.brand }

    context 'Spree::Taxonomyにname: "Brand"のデータが存在する場合' do
      it 'メソッドの戻り値.nameが"Brand"であること' do
        taxonomy_2
        expect(taxonomy_brand.name).to eq "Brand"
      end
    end

    context 'Spree::Taxonomyにname: "Brand"のデータが存在しない場合' do
      it 'メソッドの戻り値がnilであること' do
        expect(taxonomy_brand).to eq nil
      end
    end

  end

end
