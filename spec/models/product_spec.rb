require 'rails_helper'

describe Spree::Product, type: :model do

  describe '#related_products' do

    # taxonomy: 1 - taxon: 1 - product: 1
    #             ＼           product: 2
    #               taxon: 2 ∠ product: 3
    # taxonomy: 2 - taxon: 3 ∠ product: 4

    let(:taxonomy_1)        { create(:taxonomy, name: "Categories") }
    let(:taxonomy_2)        { create(:taxonomy, name: "Brand") }
    let(:product_1)         { create(:product) }
    let(:product_2)         { create(:product) }
    let(:product_3)         { create(:product) }
    let(:product_4)         { create(:product) }
    let(:taxon_1)           { create(:taxon, taxonomy: taxonomy_1) }
    let(:taxon_2)           { create(:taxon, taxonomy: taxonomy_1) }
    let(:taxon_3)           { create(:taxon, taxonomy: taxonomy_2) }
    let!(:classification_1) { create(:classification, product: product_1, taxon: taxon_1) }
    let!(:classification_2) { create(:classification, product: product_2, taxon: taxon_2) }
    let!(:classification_3) { create(:classification, product: product_3, taxon: taxon_2) }
    let!(:classification_4) { create(:classification, product: product_3, taxon: taxon_3) }
    let!(:classification_5) { create(:classification, product: product_4, taxon: taxon_3) }

    context '自分以外関連商品が無い場合' do
      it '空配列[]を返すこと' do
        product_1.reload
        expect(product_1.related_products(product_1.taxons)).to eq []
      end
    end

    context '関連商品がある場合' do
      it 'product_3と一致すること' do
        product_2.reload
        expect(product_2.related_products(product_2.taxons).count).to eq 1
        expect(product_2.related_products(product_2.taxons)).to include product_3
      end
    end

  end

  describe '#category_and_brand_related_products' do

    let(:taxonomy_1)        { create(:taxonomy, name: "Categories") }
    let(:taxonomy_2)        { create(:taxonomy, name: "Brand") }
    let(:product_1)         { create(:product) }
    let(:product_2)         { create(:product) }
    let(:product_3)         { create(:product) }
    let(:product_4)         { create(:product) }
    let(:taxon_1)           { create(:taxon, taxonomy: taxonomy_1) }
    let(:taxon_2)           { create(:taxon, taxonomy: taxonomy_1) }
    let(:taxon_3)           { create(:taxon, taxonomy: taxonomy_2) }
    let!(:classification_1) { create(:classification, product: product_1, taxon: taxon_1) }
    let!(:classification_2) { create(:classification, product: product_2, taxon: taxon_2) }
    let!(:classification_3) { create(:classification, product: product_3, taxon: taxon_2) }
    let!(:classification_4) { create(:classification, product: product_3, taxon: taxon_3) }
    let!(:classification_5) { create(:classification, product: product_4, taxon: taxon_3) }

    context '関連商品が無い場合' do
      it '空配列[]を返すこと' do
        product_1.reload
        expect(product_1.category_and_brand_related_products(3)).to eq []
      end
    end

    context '関連商品がある場合' do
      context 'Categoriesのみにある場合' do
        it 'product_3と一致すること' do
          product_2.reload
          expect(product_2.category_and_brand_related_products(3).count).to eq 1
          expect(product_2.category_and_brand_related_products(3)).to include product_3
        end
      end

      context 'Brandのみにある場合' do
        it 'product_3と一致すること' do
          product_4.reload
          expect(product_4.category_and_brand_related_products(3).count).to eq 1
          expect(product_4.category_and_brand_related_products(3)).to include product_3
        end
      end

      context 'CategoriesとBrandにある場合' do
        it 'product_2,product_4を含むこと' do
          product_3.reload
          expect(product_3.category_and_brand_related_products(3).count).to eq 2
          expect(product_3.category_and_brand_related_products(3)).to include product_2, product_4
        end
      end
    end

  end

end
